# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Kristóf Kiszel <ulysses@kubuntu.org>, 2010, 2011, 2012, 2020.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-21 02:30+0000\n"
"PO-Revision-Date: 2020-12-26 11:09+0100\n"
"Last-Translator: Kristóf Kiszel <kiszel.kristof@gmail.com>\n"
"Language-Team: Hungarian <kde-l10n-hu@kde.org>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.03.70\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Kiszel Kristóf"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "kiszel.kristof@gmail.com"

#: EditPage.cpp:180
#, kde-format
msgid ""
"The KDE Power Management System will now generate a set of defaults based on "
"your computer's capabilities. This will also erase all existing "
"modifications you made. Are you sure you want to continue?"
msgstr ""
"A KDE Energiakezelő most létrehoz egy alapértelmezett profilkészletet a "
"számítógép képességei alapján. Ez törli az összes meglévő módosítását. "
"Biztosan folytatja?"

#: EditPage.cpp:184
#, kde-format
msgid "Restore Default Profiles"
msgstr "Alapértelmezett profilok visszaállítása"

#: EditPage.cpp:261
#, kde-format
msgid "The Power Management Service appears not to be running."
msgstr "Az energiakezelő szolgáltatás úgy tűnik, nem fut."

#. i18n: ectx: attribute (title), widget (QWidget, tab)
#: profileEditPage.ui:21
#, kde-format
msgid "On AC Power"
msgstr "Hálózati áramról"

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: profileEditPage.ui:31
#, kde-format
msgid "On Battery"
msgstr "Akkumulátorról"

#. i18n: ectx: attribute (title), widget (QWidget, tab_3)
#: profileEditPage.ui:41
#, kde-format
msgid "On Low Battery"
msgstr "Alacsony töltöttségű akkumulátorról"

#~ msgid ""
#~ "The Power Management Service appears not to be running.\n"
#~ "This can be solved by starting or scheduling it inside \"Startup and "
#~ "Shutdown\""
#~ msgstr ""
#~ "Úgy tűnik, hogy az energiakezelő szolgáltatás nem fut.\n"
#~ "Ez megoldható a szolgáltatás elindításával vagy ütemezésével a "
#~ "Rendszerbeállítások „Indulás és leállítás” moduljában."

#~ msgid "Power Profiles Configuration"
#~ msgstr "Energiakezelési profilok beállítása"

#~ msgid "A profile configurator for KDE Power Management System"
#~ msgstr "Profilkészítő a KDE Energiakezelő rendszeréhez"

#~ msgid "(c), 2010 Dario Freddi"
#~ msgstr "(C) Dario Freddi, 2010."

#~ msgid ""
#~ "From this module, you can manage KDE Power Management System's power "
#~ "profiles, by tweaking existing ones or creating new ones."
#~ msgstr ""
#~ "Ebben a modulban kezelheti a KDE Energiakezelő rendszerének "
#~ "energiakezelési profiljait, szerkesztve a meglévőket vagy újakat "
#~ "létrehozva."

#~ msgid "Dario Freddi"
#~ msgstr "Dario Freddi"

#~ msgid "Maintainer"
#~ msgstr "Karbantartó"

#~ msgid "Please enter a name for the new profile:"
#~ msgstr "Adjon egy nevet az új profilnak:"

#~ msgid "The name for the new profile"
#~ msgstr "Az új profil neve"

#~ msgid "Enter here the name for the profile you are creating"
#~ msgstr "Itt adja meg a létrehozandó profil nevét"

#~ msgid "Please enter a name for this profile:"
#~ msgstr "Adjon egy nevet az új profilnak:"

#~ msgid "Import Power Management Profiles"
#~ msgstr "Energiakezelési profilok importálása"

#~ msgid "Export Power Management Profiles"
#~ msgstr "Energiakezelési profilok exportálása"

#~ msgid ""
#~ "The current profile has not been saved.\n"
#~ "Do you want to save it?"
#~ msgstr ""
#~ "A jelenlegi profil nincs elmentve.\n"
#~ "El szeretné menteni?"

#~ msgid "Save Profile"
#~ msgstr "Profil mentése"

#~ msgid "New Profile"
#~ msgstr "Új profil"

#~ msgid "Delete Profile"
#~ msgstr "Profil törlése"

#~ msgid "Import Profiles"
#~ msgstr "Profilok importálása"

#~ msgid "Export Profiles"
#~ msgstr "Profilok exportálása"

#~ msgid "Edit Profile"
#~ msgstr "Profil szerkesztése"
